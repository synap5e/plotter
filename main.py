"""

This file is part of https://github.com/synap5e/Plotter and is released under the MIT license:
http://opensource.org/licenses/MIT 

The License follows:

Copyright (C) 2012 Simon Pinfold

Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
and associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to 
do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""

import serial, time, sys

def readLine(lines):
    line = ""
	# Keep reading untill the timeout is reached (1s)
    while True:
        line = ser.readline()
        if not line:
            break
        lines.append(line)
        print line[:-1]


# hpgl files are very high resolution, 1mm equates to 40 units
# This resolution defines one "step" of a rotary encoder to be equal to 40 * 5 hpgl units, or 5 mm
# this is not to scale
res = 40 * 5;

# hpgl file use absolute positioning, while we use relative
x = 0
y = 0

# hardcoded file to read
f = file("board.plt")
data = f.read()
f.close()

send_data = list()

# Zero the counters
send_data.append("z")

# split the hpgl data into each command
cmds = data.split(";")
for cmd in cmds:
	# first 2 letters is the command key
    command = cmd[:2]
	# the rest is data/args for that command
    args = cmd[2:]
    command_string = ""
	# only read pen up or pen down commands
    if command == "PU" or command == "PD":
        if command == "PU":
			# Pen Up equates to 'e' on plotter
            command_string += "e"
        else:
			# Pen Up equates to 'c' on plotter
            command_string += "c"
        points = args.split(",")
		# every 2n numerical args are x coords
        x_points = points[::2]
		# and every 2n+1 are y coords
        y_points = points[1::2]
        for i in range(0, len(x_points)):
            old_x = x
            old_y = y
            x = int(x_points[i])
            y = int(y_points[i])
            
			# absolute positioning to relative
            if x > old_x:
                command_string += "d" * ((x - old_x) / res)
            else:
                command_string += "a" * ((old_x - x) / res)
            if y > old_y:
                command_string += "w" * ((y - old_y) / res)
            else:
                command_string += "s" * ((old_y - y) / res)
            
        send_data.append(command_string)

send_data.append("e")
send_data.append("r")

#Debugging
print send_data

lines = list()

print "Finding serial device"
ser = None
# Just iterate the first 16 devices
for i in range(0, 15):
    try:
        ser = serial.Serial(1, 9600, timeout=1)
        break
    except serial.SerialException:
        pass
if not ser:
    print "ERROR: could not open serial device"
    sys.exit(1)
else:
    print "Using serial port " + str(ser.port)     

# When serial is attatched the arduino restarts, give it some time
time.sleep(5)
# Read until the arduino has nothing to say
readLine(lines)

for command_string in send_data:
    print ">> " + command_string
    ser.write(command_string)
	# Make completly sure the command has finished
    readLine(lines)
    readLine(lines)
    
# Clean up
ser.close()

