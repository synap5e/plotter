/*	

This file is part of https://github.com/synap5e/Plotter and is released under the MIT license:
http://opensource.org/licenses/MIT 

The License follows:

Copyright (C) 2012 Simon Pinfold

Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
and associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to 
do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

#include <Encoder.h>
#include <Servo.h> 
#include <Bounce.h>

/* -------------------- CONTROL -------------------- */

int x_resolution = 1;
int y_resolution = 1;

/* -------------------- ENCODERS -------------------- */

// Change these pin numbers to the pins connected to your encoder.
//   Best Performance: both pins have interrupt capability
//   Good Performance: only the first pin has interrupt capability
//   Low Performance:  neither pin has interrupt capability

// pins 2 & 3 have interrupt capability

Encoder pos_x(2, 4); // Poor Performance
Encoder pos_y(3, 5); // Best Performance



/* -------------------- BUTTONS -------------------- */

#define X_INC_PIN 11
Bounce x_inc_button = Bounce( X_INC_PIN,5 ); // Pin 11, 5ms bounce time
#define X_DEC_PIN 12
Bounce x_dec_button = Bounce( X_DEC_PIN,5 ); // Pin 12, 5ms bounce time
#define Y_INC_PIN A4
Bounce y_inc_button = Bounce( Y_INC_PIN,5 ); // Pin A4, 5ms bounce time
#define Y_DEC_PIN 13
Bounce y_dec_button = Bounce( Y_DEC_PIN,5 ); // Pin 13, 5ms bounce time

#define SOLENOID_BUTTON_PIN A3
Bounce solenoid_button = Bounce( SOLENOID_BUTTON_PIN,5 ); 

/* -------------------- SERVOS -------------------- */

#define Y_SERVO_PIN 9
Servo y_servo;  // Pin 9
#define X_SERVO_PIN 10
Servo x_servo;  // Pin 10

// Continious rotation servo square wave values
#define SERVO_STOP 90



/* -------------------- SOLENOID -------------------- */

#define SOLENOID_PIN 8
int solenoid = SOLENOID_PIN; // Pin 8




void setup() {
  Serial.begin(9600);
  Serial.println("XY Plotter by Simon Pinfold");
  
  pinMode(X_INC_PIN, INPUT);  
  pinMode(X_DEC_PIN, INPUT);  
  pinMode(Y_INC_PIN, INPUT);  
  pinMode(Y_DEC_PIN, INPUT);
  
  pinMode(SOLENOID_PIN, OUTPUT);
  
  y_servo.attach(Y_SERVO_PIN);
  x_servo.attach(X_SERVO_PIN);
  y_servo.write(SERVO_STOP);
  x_servo.write(SERVO_STOP);
}

// The target values 
int x_target = 0;
int y_target = 0;

int solenoid_value = 0;

// Keep track of the intended direction to prevent "bouncing" 
int x_last_direction = 0;
int y_last_direction = 0;

// internals for serial logging
int x_t_l = 0;
int x_p_l = 0;
int y_t_l = 0;
int y_p_l = 0;
int s_l = 0;


void loop() {
  // Button internals
  x_inc_button.update();
  x_dec_button.update();
  y_inc_button.update();
  y_dec_button.update();
  solenoid_button.update();
  
 
  int cont, input;
  do {
    cont = 0;
    if (x_inc_button.read() || input == 's') { x_target+=x_resolution; x_last_direction = 1; }
    if (x_dec_button.read() || input == 'w') { x_target-=x_resolution; x_last_direction = -1; }
    if (y_inc_button.read() || input == 'a') { y_target+=y_resolution; y_last_direction = 1; }
    if (y_dec_button.read() || input == 'd') { y_target-=y_resolution; y_last_direction = -1; }
    if (input == 'e') { solenoid_value = 0; }
    if (input == 'c') { solenoid_value = 1; }
    if (input == 'z') { pos_y.write(0); pos_x.write(0); x_target = 0; y_target = 0;}
    if (input == 'r') { x_target = 0; y_target = 0; y_last_direction=(2 * (pos_y.read() < y_target)) - 1; x_last_direction=(2 * (pos_x.read() < x_target)) - 1; } // Sey up the directions accordingly
    if (solenoid_button.risingEdge() || input == 'q') { solenoid_value = !solenoid_value;}
    if (Serial.available()){
      input = Serial.read();
      cont = 1;
    }
  } while (cont);
  
  digitalWrite(solenoid, solenoid_value);

  int i = 0;

  if (pos_x.read() < x_target && x_last_direction == 1){
    x_servo.write(SERVO_STOP - 15);
    while (pos_x.read() < x_target && i++ < 450){
      delay(1);
    }
  } else if (pos_x.read() > x_target && x_last_direction == -1){
    x_servo.write(SERVO_STOP + 15);//x_delta );
    while(pos_x.read() > x_target && i++ < 450){
      delay(1);
    }
  }
  x_servo.write(SERVO_STOP);
  
  i = 0;
  if (pos_y.read() < y_target && y_last_direction == 1){
    y_servo.write(SERVO_STOP - 3);
    while (pos_y.read() < y_target && i++ < 450){
      delay(1);
    }
  } else if (pos_y.read() > y_target && y_last_direction == -1){
    y_servo.write(SERVO_STOP + 3);//y_delta );
    while(pos_y.read() > y_target && i++ < 450){
      delay(1);
    }
  }
  y_servo.write(SERVO_STOP);

/*

  int x_delta = min(abs(pos_x.read() - x_target), 15) * 6; // The speed could be relitive to the distane away from the target
  /* x_last_direction is used so that the servo will only move in the direction it has been instructed
     rather than attempt to move to the target in whatever direction possible. This prevents the servo
     "bouncing" the arm backwards and forwards if it goes slightly over. */  /*
  if (pos_x.read() < x_target && x_last_direction == 1){ 
    x_servo.write(SERVO_STOP - 15);//x_delta);
  } else if (pos_x.read() > x_target && x_last_direction == -1){
    x_servo.write(SERVO_STOP + 15);//x_delta );
  } else {
    x_servo.write(SERVO_STOP);
  }
  
  
  int y_delta = min(abs(pos_y.read() - y_target), 15) * 6; // The speed could be relitive to the distane away from the target
  // See above for y_last_direction explination
  if (pos_y.read() < y_target && y_last_direction == 1){
    y_servo.write(SERVO_STOP - 5);//y_delta);
  } else if (pos_y.read() > y_target && y_last_direction == -1){
    y_servo.write(SERVO_STOP + 5);//y_delta );
  } else {
    y_servo.write(SERVO_STOP);
  }*/

  
  if (x_t_l != x_target || x_p_l != pos_x.read() || y_t_l != y_target || y_p_l != pos_y.read() || s_l != solenoid_value){
    x_t_l = x_target;
    x_p_l = pos_x.read();    
    y_t_l = y_target;
    y_p_l = pos_y.read();
    s_l = solenoid_value;
    
    Serial.print("x_target: " );
    Serial.print(x_target);
    Serial.print("\tx_pos: ");
    Serial.print(pos_x.read());
   // Serial.print("\tx_delta: ");
    //Serial.print(x_delta);
    Serial.print("\t|\t");
    
    Serial.print("y_target: " );
    Serial.print(y_target);
    Serial.print("\ty_pos: ");
    Serial.print(pos_y.read());
  //  Serial.print("\ty_delta: ");
   // Serial.print(y_delta);
    Serial.print("\t|\t");
    
    Serial.print("Solenoid: ");
    Serial.print(solenoid_value);
    
    Serial.println();
    }
    

}
